use strict;
use warnings;

use Data::Dumper qw(Dumper);

my $tokens = tokenize( 'splitter-tokenizer.pl' );
print sprintf(">>> %s\n", $_) for @{$tokens // []};

sub tokenize {
    my ($file_path, %syntax) = @_;

    open my $fin, '<', $file_path or die sprintf(q{Couldn't open '%s': %s}, $file_path, $!);

    my %start_delim_to_end_delim = (
        '(' => ')',
        '[' => ']',
        '{' => '}',
        '<' => '>',
    );
    $start_delim_to_end_delim{$_} = $_ for ('"', "'", '/');

    my @tokens;

    my $char;
    my $prev_char; # Just for the record

    my @piped_chars;
    my $word_buffer = '';
    my $inside_word = 0;

    my $local_getc = sub {
        my ($fin_local) = @_;
        my $chrz = getc($fin_local);
        return $chrz;
    };

    #
    # The default parser behavior
    #

    my $is_word_char = $syntax{is_word_char} // sub {
        my ($char) = @_;
        return $char =~ /[a-zA-Z\_0-9]/;
    };
    my $operators = $syntax{operators} // ['(', ')', '/', '=~', ';', '{', '}', '[', ']', '$', '//=', '//', '@', '=', '@_', '::', 'q{', 'qq{', 'q(', 'qq(', 'q/', 'qq/', 'q<', 'qq<'];
    my $is_string_opener = $syntax{is_string_opener} // sub {
        my ($str) = @_;
        my $res = ($str =~ /^qq?[\[\{<]$/) || ($str eq q{'}) || ($str eq q{"}) || ($str eq q{/});
        return $res;
    };
    my $is_comment_opened = $syntax{is_comment_opened} // sub {
        return 0;
    };
    my $process_a_string = $syntax{process_a_string} // sub {
        my $the_latest_token = '';

        # Let's check if a string might have started
        if(int(@tokens) && $is_string_opener->($tokens[int(@tokens) - 1])) {
            $the_latest_token = pop @tokens;
        }
        elsif(!$is_string_opener->($char)) {
            return; # Nopes, no string started
        }
        # Yes, a string just started

        my $start_delim = $the_latest_token ? substr($the_latest_token, -1, 1) : $char;
        my $end_delim = $start_delim_to_end_delim{$start_delim} or die sprintf(q{Can't figure end_delim for start_delim='%s'}, $start_delim);

        my $string_buffer = $the_latest_token . $char;
        while(defined(my $newchar = $local_getc->($fin))) {
            if($newchar eq '\\') {
                $string_buffer .= $newchar;
                $newchar = $local_getc->($fin);
                $string_buffer .= $newchar if $newchar;
                next;
            }
            $string_buffer .= $newchar;
            if($newchar eq $end_delim) {
                push @tokens, $string_buffer;
                $string_buffer = '';
                last;
            }
        }

        if($string_buffer) {
            push @tokens, $string_buffer;
        }

        return 1;
    };
    my $process_a_comment = $syntax{process_a_comment} // sub {
        if($char eq '#') {
            my $newbuffer = $char;
            my $newchar;
            while(defined($newchar = $local_getc->($fin))) {
                if($newchar eq "\n") {
                    push @tokens, $newbuffer;
                    return 1;
                }
                $newbuffer .= $newchar;
            }

            if(!defined($newchar)) {
                push @tokens, $newbuffer;
                return 1;
            }
        }
        return;
    };

    #
    # Prep the operators
    #
    my $multichar_operators = {};
    my $multichar_operator_maxlengths = {};
    my $singlechar_operators = {};

    for my $operator (@{$operators // []}) {
        my $operator_length = length($operator);
        if($operator_length == 1) {
            $singlechar_operators->{$operator} = 1;
        }
        else {
            my $the_first_char = substr($operator, 0, 1);
            $multichar_operators->{$the_first_char} //= {};
            $multichar_operators->{$the_first_char}{$operator} = 1;

            $multichar_operator_maxlengths->{$the_first_char} //= $operator_length;

            if($operator_length > $multichar_operator_maxlengths->{$the_first_char}) {
                $multichar_operator_maxlengths->{$the_first_char} = $operator_length;
            }
        }
    }

    while(defined($char = int(@piped_chars) ? shift @piped_chars : $local_getc->($fin))) {

        # Has a tring started? Process it!
        next if($process_a_string->()); # TODO: optimize the calling rate
        next if($process_a_comment->());

        # Process multichar operators
        if(my $multichar_operator_candidates = !$inside_word && $multichar_operators->{$char}) {
            my $fetch_length = $multichar_operator_maxlengths->{$char};
            my $multichar_operator_buffer = $char;
            my $found_operator;
            for(my $i=1; $i<$fetch_length; $i++) {
                my $newchar = $local_getc->($fin);
                push @piped_chars, $newchar;
                $multichar_operator_buffer .= $newchar;
                if($multichar_operator_candidates->{$multichar_operator_buffer}) {
                    $found_operator = $multichar_operator_buffer;
                }
            }

            if($found_operator) {
                push @tokens, $found_operator;
                my $discard_piped = length $found_operator;
                shift @piped_chars for (2..$discard_piped);
                $prev_char = $char;
                next;
            }
        }

        # Words vs. everything else
        if($is_word_char->($char)) {
            $inside_word = 1;
            $word_buffer .= $char;

            $prev_char = $char;
            next;
        }
        elsif($inside_word) {
            push @tokens, $word_buffer;
            $inside_word = 0;
            $word_buffer = '';
        }

        # push @tokens, $char; # if($singlechar_operators->{$char})

        push @tokens, $char unless $char =~ /\s/;

        $prev_char = $char;
    }

    return \@tokens;
}
